const app = require("express")();
const http = require("http").createServer(app);

const port = 3000;

app.get("/", function(req, res) {
  res.send("Le serveur fontionne!");
});

http.listen(port, () => {
  console.log(`Server running at http://*:${port}/`);
});
